#Seminario ZMQ

Realizado por José Carlos Montañez Aragón y José Crespo.

###Idea del proyecto
Se desea implementar un sistema de copias de seguridad para una red local de ordenadores. Este sistema estará implementado en Python haciendo uso de las librerías y herramientas de ZeroMQ.

###Documentación

La documentación se encuentra en el repositorio, en la carpeta principal. [Aquí está un enlace al mismo](https://github.com/Montagon/SD-ZMQ/blob/master/Documentacion.pdf).
###Ejecución

Para ejecutarlo, hacen falta dos comandos:

**Cliente**: *python cliente.py*. Con esto lanzamos cada uno de los nodos cliente, que serán los que monitorizarán una carpeta y mandarán los archivos al servidor central.

**Servidor**: *python servidor.py*. Con esto lanzamos el servidor. Cada cierto tiempo realizará una petición a los nodos de la red que estén en funcionamiento generando la transmisión de los ficheros para su almacenaje.

####Configuración

Tenemos varias variables de configuración:

En el apartado del **cliente** tenemos que definir el directorio que se va a monitorizar:

![](http://i.imgur.com/h8IGoPi.png "Configuración cliente")

En el apartado del servidor, tenemos la variable de configuración del tiempo que tardará en realizar las peticiones a los clientes. La IP con los tres primeros octetos de la red en la que se trabaja. Y por último los valores de los nodos que se van a utilizar en la red. En caso de que sea toda la red, basta con poner *rango = range(1,255)*:

![](http://i.imgur.com/eRitZmq.png "Configuración servidor")

También adjuntamos una captura mostrando un ejemplo de ejecución. ![ ](http://i.imgur.com/IPUYd6S.png  "Ejemplo de ejecución")

Además se ha realizado la grabación de un vídeo para mostrar el funcionamiento del mismo en un entorno simulado acompañado de una explicación de la configuración:

[http://www.dailymotion.com/video/x2t1hef_zmq-sd_tech](http://www.dailymotion.com/video/x2t1hef_zmq-sd_tech) 



