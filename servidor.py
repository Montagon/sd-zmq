#!/usr/bin/python2
# -*- coding: utf-8 -*-
"""
    Realizará una petición cada TIME tiempo a los nodos que estén en la red,
    de manera que cada uno de ellos subirá los ficheros nuevos o modificados.
"""

import zmq
import os
import time
import sys
import subprocess

# Variables de configuración
TIME = 5 #en segundos
IP = "192.168.1." # Cambiar por la IP de la red local
rango = [9, 12]

def main():
    while True:
        time.sleep(TIME)
        #Enviar petición a nodos hijos
        print "----------------------------------------------"
        print "Nuevo evento de transmisión"
        for ping in rango: #Todos los nodos de la red
            time.sleep(2)
            address = "192.168.1." + str(ping)
            res = subprocess.call(['ping', '-c', '1', '-W', '1', address], stdout=open(os.devnull, "wb"), stderr=open(os.devnull, "wb"))
            if res == 0:
                #No ha habido error, nos conectamos a cada uno.
                print "Ping a la dirección ", address, " correcto."
                context = zmq.Context()
                socket = context.socket(zmq.REQ)
                socket.connect('tcp://'+address+':4545')


                dest = open(os.path.basename('listaFicheros'), 'w+')
                # Pedimos la lista.
                socket.send('listaFicheros')


                while True:
                    # Start grabing data
                    data = socket.recv()
                    # print data
                    # Write the chunk to the file
                    dest.write(data)
                    if not socket.getsockopt(zmq.RCVMORE):
                        # If there is not more data to send, then break
                        break

                dest.close()
                print "Obtenido fichero 'listaFicheros'"
                time.sleep(0.5)
                # Ya tenemos la lista
                lista = open('listaFicheros', 'r')

                res = subprocess.call(['mkdir', '-p', str(ping)])
                os.chdir(str(ping))

                for elemento in lista:
                    elemento = elemento.replace('\n', '')
                    elemento = elemento.replace('\r', '')
                    dest = open(os.path.basename(str(ping) + '/' + elemento), 'w+')
                    socket.send(elemento)

                    while True:
                        # Start grabing data
                        data = socket.recv()
                        # print data
                        # Write the chunk to the file
                        dest.write(data)
                        if not socket.getsockopt(zmq.RCVMORE):
                            # If there is not more data to send, then break
                            break

                    dest.close()
                    print "Obtenido fichero " + elemento
                    time.sleep(0.5)

                print "-- FIN CONEXION --"
                os.chdir('..')
                res = subprocess.call(['rm', 'listaFicheros'])



if __name__ == '__main__':
    #get_file(sys.argv[1])
    main()
