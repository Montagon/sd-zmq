#!/usr/bin/python2
# -*- coding: utf-8 -*-

"""
    Cliente.

    En base a un directorio de monitorización, estará a la espera de nuevas
    peticiones para enviar los ficheros de los que se quiere hacer una copia
    de seguridad. Existen dos opciones en el algoritmo:

        - Generación de un fichero con la lista de archivos de los que se debe
        realizar una copia de seguridad y posterior envío del mismo.

        - Envío de peticiones aisladas de ficheros. Los mismos que se deben
        encontrar dentro del archivo creado previamente.
        
"""

import zmq
import os
import subprocess

# Directorio de monitorización
DIR = 'prueba/'

def generarListaFicheros():
    '''
    Función que genera un fichero 'listaFicheros' con todos los
    ficheros que se encuentren en el directorio de trabajo.
    '''
    comando = 'ls -p ' + DIR + '| grep -v / > listaFicheros'
    archivos = subprocess.check_output([comando], shell=True)

def enviarFichero(fichero, sock):
    '''
    Función que envía un fichero, que es argumento de entrada.
    '''
    fn = open(fichero, 'rb')
    stream = True

    # Empieza la lectura del archivo
    while stream:
        stream = fn.read(128)
        if stream:
            sock.send(stream, zmq.SNDMORE)
        else:
            sock.send(stream)

    print "Archivo " + fichero + " enviado"


def server():
    '''
    Servidor. En este caso actúa como cliente. Espera la petición del servidor
    y envía el archivo correspondiente.
    '''

    context = zmq.Context(1)
    sock = context.socket(zmq.REP)
    sock.bind('tcp://*:4545')

    print "Servidor cliente en ejecucion"
    # Bucle principal
    while True:
        # Obtenemos el mensaje
        msg = sock.recv()

        # Si el mensaje es 'listaFicheros' tenemos que generar dicho archivo
        # y enviarlo. En caso contrario, enviamos el archivo que ha sido pedido
        # en el directorio monitorizado
        if msg == 'listaFicheros':
            generarListaFicheros()
            enviarFichero('listaFicheros', sock)
        else:
            # Concatenamos el directorio que se est
            msg = DIR + msg
            # Comprobamos que el fichero existe
            if not os.path.isfile(msg):
                sock.send('')
                print "El archivo no se encuentra en el sistema"
                continue

            enviarFichero(msg, sock)

if __name__ == '__main__':
    server()
